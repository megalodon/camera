from gpiozero import Button
from picamera import PiCamera
from datetime import datetime
from signal import pause

right_button = Button(2)
left_button = Button(3)
camera = PiCamera()


def capture():
    date = datetime.now().isoformat()
    camera.capture('/home/pi/Pictures/%s.jpg' % date)


left_button.when_pressed = camera.start_preview
left_button.when_released = camera.stop_preview
right_button.when_pressed = capture

pause()
