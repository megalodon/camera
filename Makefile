camera:
	python3 camera.py

sync-remote:
	rsync --delete -r --progress ../ pi@raspberrypi.local:/home/pi/megalodon/

sync-local:
	rsync --delete -r --progress pi@raspberrypi.local:/home/pi/megalodon/ ../

install-requirements:
	sudo apt update && sudo apt install python3-gpiozero python-picamera python3-picamera
